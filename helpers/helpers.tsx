import { FirstLevelMenuItem } from '../interface/menu.interface';
import { TopLevelCategory } from '../interface/page.interface';

import IconCourses from './icons/Courses.svg';
import IconServices from './icons/Services.svg';
import IconBooks from './icons/books.svg';
import IconGoods from './icons/goods.svg';

export const firstLevelMenu: FirstLevelMenuItem[] = [
	{ route: 'courses', name: 'Курсы', icon: <IconCourses />, id: TopLevelCategory.Courses },
	{ route: 'services', name: 'Сервисы', icon: <IconServices />, id: TopLevelCategory.Services },
	{ route: 'books', name: 'Книги', icon: <IconBooks />, id: TopLevelCategory.Books },
	{ route: 'products', name: 'Продукты', icon: <IconGoods />, id: TopLevelCategory.Products },
];

export const priceRu = (price: number): string => price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ').concat(' ₽');

export const declinationOfNum = (num: number, titles: [string, string, string]): string => {
	const cases = [2, 0, 1, 1, 1, 2];
	return titles[(num % 100 > 4 && num % 100 < 20) ? 2 : cases[(num % 10 < 5) ? num % 10 : 5]];
}