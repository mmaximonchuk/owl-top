// import Head from 'next/head';
// import Image from 'next/image';
import React, { useState } from 'react';
import { GetStaticProps } from 'next';
import { ParsedUrlQuery } from 'querystring';
import axios from 'axios';

import { MenuItem } from '../interface/menu.interface';
import { withLayout } from '../layout/Layout';
import { Button, Input, Rating, Tag, Textarea, Title, Typography } from '../components';
import { API } from '../helpers/api';

function Home({ menu, firstCategory }: HomeProps): JSX.Element {
  const [rating, setRating] = useState<number>(4);

  return (
    <>
      Hi, welcom to Owl-top the best top for courses!
      {/* <Title tag="h1">Text</Title>
      <Button appearance="primary" className="awawawaw" arrow="right">awdawdaw</Button>
      <Button appearance="ghost" arrow="right">awdawdaw</Button>
      <Typography tag="p" size="s">Typography par</Typography>
      <Typography tag="p" size="m">Typography par</Typography>
      <Typography tag="p" size="l">Typography par</Typography>
      <Tag size="m" color="ghost">Typography span</Tag>
      <Tag size="s" color="red">Typography span</Tag>
      <Tag size="m" color="green">Typography span</Tag>
      <Tag size="s" color="primary">Typography span</Tag>
      <Tag size="s" color="gray">Typography span</Tag>
      <Rating rating={rating} setRating={setRating} isEditable />
      <Input placeholder="Test" />
      <Textarea placeholder="Test textArea" /> */}
    </>
  );
}

export default withLayout(Home);

export const getStaticProps: GetStaticProps<HomeProps, ParsedUrlQuery> = async () => {
  const firstCategory = 0;
  const { data: menu } = await axios.post<MenuItem[]>(API.topPage.find, { firstCategory });
  return { 
    props: {
      menu,
      firstCategory,
    }
  };
};

interface HomeProps extends Record<string, unknown> {
  menu: MenuItem[],
  firstCategory: number;
}