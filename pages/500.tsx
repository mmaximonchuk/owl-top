import React from 'react';
import { Title } from '../components';
import { withLayout } from '../layout/Layout';

function Error500(): JSX.Element {
	return (
		<Title tag="h1">
			Ошибка 500
		</Title>
	);
}

export default withLayout(Error500);
