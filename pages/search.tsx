// import Head from 'next/head';
// import Image from 'next/image';
import React from 'react';
import { GetStaticProps } from 'next';
import { ParsedUrlQuery } from 'querystring';
import axios from 'axios';

import { MenuItem } from '../interface/menu.interface';
import { withLayout } from '../layout/Layout';
import { API } from '../helpers/api';

function Search({ menu }: HomeProps): JSX.Element {
	// https://courses-top.ru/api/top-page/find

	return (
		<>
			Hi, welcom to the search page{')'}
		</>
	);
}

export default withLayout(Search);

export const getStaticProps: GetStaticProps<HomeProps, ParsedUrlQuery> = async () => {
	const firstCategory = 0;
	const { data: menu } = await axios.post<MenuItem[]>(API.topPage.find, { firstCategory });
	return {
		props: {
			menu,
			firstCategory,
		}
	};
};

interface HomeProps extends Record<string, unknown> {
	menu: MenuItem[],
	firstCategory: number;
}