import React from 'react';
import { Title } from '../components';
import { withLayout } from '../layout/Layout';

export function Error404(): JSX.Element {
	return (
		<Title tag="h1">
			Ошибка 404
		</Title>
	);
}

export default withLayout(Error404);
