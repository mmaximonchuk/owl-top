import React, { useEffect } from 'react';
import { AppProps } from 'next/dist/next-server/lib/router/router';
import Head from 'next/head';
// import ym from 'react-yandex-metrika';
import ReactGA from 'react-ga';
import '../styles/globals.css';

function MyApp({ Component, pageProps, router }: AppProps): JSX.Element {

  const initGA = () => {
    ReactGA.initialize('UA-00000000-1');
    ReactGA.pageview(window.location.pathname + window.location.search);
    console.log(window.location.pathname + window.location.search);
  };

  useEffect(() => {
    initGA();
  }, []);

  // router.events.on('routeChangeComplete', (url: string) => {
  //   if(typeof window !== 'undefined') {
  //     ym('hit', url);
  //   }
  // });

  return (<>
    <Head>
      <title>OwlTop - the best top</title>
      <link rel="icon" href="/favicon.ico" />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" />
      <link rel="preconnect" href="https://mc.yandex.ru" />
      <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300;400;500;700&display=swap" rel="stylesheet" />
      <meta property="og:url" content={process.env.NEXT_PUBLIC_DOMAIN + router.asPath} />
      <meta property="og:locale" content="ru_RU" />

    </Head>
    {/* <YMInitializer
      accounts={[]}
      options={{webvisor: true, defer: true}}
      version="2"
    /> */}
    <Component {...pageProps} />
  </>);
}

export default MyApp;
