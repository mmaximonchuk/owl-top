import React from 'react';
import { GetStaticPaths, GetStaticProps, GetStaticPropsContext } from 'next';
import Head from 'next/head';
import { ParsedUrlQuery } from 'querystring';
import axios from 'axios';
import { Error404 } from '../404';

import { TopLevelCategory, TopPageModel } from '../../interface/page.interface';
import { ProductModel } from '../../interface/product.interface';
import { MenuItem } from '../../interface/menu.interface';

import { withLayout } from '../../layout/Layout';
import { firstLevelMenu } from '../../helpers/helpers';
import { TopPageComponent } from '../../page-components';
import { API } from '../../helpers/api';

function TopPage({ firstCategory, page, products }: TopPageProps): JSX.Element {
	if (!page || !products) {
		return <Error404 />;
	}
	
	return (
		<>
			<Head>
				<title>{page.metaTitle}</title>
				<meta name="description" content={page.metaDescription} />
				<meta property="og:title" content={page.metaTitle} />
				<meta property="og:description" content={page.metaDescription} />
				<meta property="og:type" content="article" />
			</Head>
			<TopPageComponent
				page={page}
				products={products}
				firstCategory={firstCategory}
			/>
		</>
	);
}

export default withLayout(TopPage);


export const getStaticPaths: GetStaticPaths = async () => {
	let paths: string[] = [];
	for (const m of firstLevelMenu) {
		const { data: menu } = await axios.post<MenuItem[]>(API.topPage.find, { firstCategory: m.id });
		paths = paths.concat(menu.flatMap(menuItem => menuItem.pages.map(page => `/${m.route}/${page.alias}`)));
	}
	// console.log('paths: ', paths);
	return {
		paths,
		fallback: false
	};
};

export const getStaticProps: GetStaticProps<TopPageProps, ParsedUrlQuery> = async ({ params }: GetStaticPropsContext<ParsedUrlQuery>) => {
	if (!params) {
		return {
			notFound: true,
		};
	}
	const firstCategoryItem = firstLevelMenu.find(menu => menu.route === params.type);
	if (!firstCategoryItem) {
		return {
			notFound: true,
		};
	}
	try {
		const { data: menu } = await axios.post<MenuItem[]>(API.topPage.find,
			{
				firstCategory: firstCategoryItem.id
			});

		if (menu.length === 0) {
			return {
				notFound: true,
			};
		}
		const { data: page } = await axios.get<TopPageModel>(API.topPage.byAlias + params.alias);
		const { data: products } = await axios.post<ProductModel[]>(API.product.find, { category: page.category, limit: 10 });
		return {
			props: {
				menu,
				firstCategory: firstCategoryItem.id,
				page,
				products,
			}
		};
	} catch {
		return {
			notFound: true,
		};
	}

};

interface TopPageProps extends Record<string, unknown> {
	menu: MenuItem[],
	firstCategory: TopLevelCategory;
	page: TopPageModel;
	products: ProductModel[];
}