import React, { useEffect, useReducer } from "react";
// import cn from "classnames";
import { TopPageComponentProps } from './TopPageComponent.props';
import { TopLevelCategory } from '../../interface/page.interface';
import { Advantages, HhData, Product, Sort, Tag, Title } from '../../components';

import styles from "./TopPageComponent.module.css";
import { SortEnum } from '../../components/Sort/Sort.props';
import { sortReducer } from './sort.reducer';
// import { useScrollY } from '../../hooks/useScrollY';
// import { ProductModel } from '../../interface/product.interface';

export function TopPageComponent({ products, firstCategory, page }: TopPageComponentProps): JSX.Element {
	const [{ products: sortedProducts, sort }, dispatchSort] = useReducer(sortReducer, { products, sort: SortEnum.Rating });
	// const y = useScrollY();

	const setSort = (sort: SortEnum) => {
		dispatchSort({ type: sort });
	};

	useEffect(() => {
		dispatchSort({type: SortEnum.ResetProducts, payload: products});
	}, [products]);
	
	return (
		<div className={styles.wrapper}>
			<div className={styles.title}>
				<Title tag="h1">{page.title}</Title>
				{products && <Tag aria-label={products.length + 'элементов'} color="gray" size="m">{products.length}</Tag>}
				<Sort sort={sort} setSort={setSort} />
			</div>
			<div role="list">
				{products && sortedProducts.map(p => <Product role="list-item" layout key={p._id} product={p} />)}

			</div>

			<div className={styles.hhTitle}>
				<Title tag="h2">Вакансии - {page.category}</Title>
				<Tag color="red" size="m">hh.ru</Tag>
			</div>

			{Boolean(firstCategory == TopLevelCategory.Courses) && page.hh && <HhData {...page.hh} />}
			{page.advantages && page.advantages.length > 0 && (
				<Advantages advantages={page.advantages} seoText={page.seoText} tags={page.tags} />
			)}

		</div>
	);
}

// export const getStaticProps: GetStaticProps<TopPageComponentProps, ParsedUrlQuery> = ({ products, page, firstCategory }: GetStaticPropsContext<ParsedUrlQuery>) => {
// 	return {
// 		props: {
// 			products,
// 			page,
// 			firstCategory
// 		}
// 	};
// };
