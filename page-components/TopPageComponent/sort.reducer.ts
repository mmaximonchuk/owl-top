import { SortEnum } from '../../components/Sort/Sort.props';
import { ProductModel } from '../../interface/product.interface';

export type SortActions = {
	type: SortEnum.Price
} | {
	type: SortEnum.Rating
} | {
	type: SortEnum.ResetProducts,
	payload?: ProductModel[]
};

export interface SortReducerState {
	sort: SortEnum;
	products: ProductModel[];
}

export const sortReducer = (state: SortReducerState, action: SortActions): SortReducerState => {
	switch (action.type) {
		case SortEnum.Rating:
			return {
				sort: SortEnum.Rating,
				products: state.products.sort((a, b) => a.initialRating > b.initialRating ? -1 : 1),
			};
		case SortEnum.Price: 
			return {
				sort: SortEnum.Price,
				products: state.products.sort((a, b) => a.price > b.price ? 1 : -1)
			};
		case SortEnum.ResetProducts:
			return {
				...state,
				products: action?.payload !== undefined ? [...action.payload] : state.products
			};
		default:
			throw new Error('Unknown sort action');
	}
};
