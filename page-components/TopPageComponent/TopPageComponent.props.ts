import { ProductModel } from './../../interface/product.interface';
import { TopPageModel } from './../../interface/page.interface';
import { TopLevelCategory } from '../../interface/page.interface';

export interface TopPageComponentProps extends Record<string, unknown> {
	firstCategory: TopLevelCategory;
	page: TopPageModel;
	products: ProductModel[];
}