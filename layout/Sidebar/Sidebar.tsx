import React from 'react';
import cn from 'classnames';
import NextLink from 'next/link';

import { SidebarProps } from './Sidebar.props';
import { Search } from '../../components';
import Menu from '../Menu/Menu';

import Logo from '../Logo.svg';

import styles from './Sidebar.module.css';

function Sidebar({ className, ...props }: SidebarProps): JSX.Element {
	return (
		<nav className={cn(styles.navbar, className)} {...props}>
			<NextLink href="/">
				<a>
					<Logo className={styles.logo} />
				</a>
			</NextLink>
			<Search	/>
			<Menu />
		</nav>
	);
}

export default Sidebar;

