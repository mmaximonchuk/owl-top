import React, { useContext, KeyboardEvent, useState } from 'react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import cn from 'classnames';
// import { format } from 'date-fns';
import { motion } from 'framer-motion';
import { FirstLevelMenuItem, PageItem } from '../../interface/menu.interface';

import { Typography } from '../../components';
import { AppContext } from '../../context/app.context';
import { firstLevelMenu } from '../../helpers/helpers';

import styles from './Menu.module.css';


function Menu(): JSX.Element {
	const { menu, setMenu, firstCategory } = useContext(AppContext);
	const [announce, setAnnounce] = useState<'closed' | 'opened' | undefined>();
	const router = useRouter();
	const vatiants = {
		visible: {
			marginBottom: 10,
			transition: {
				when: 'beforeChildren',
				staggerChildren: 0.1,
			}
		},
		hidden: {
			marginBottom: 0,
		},
	};
	const vatiantsChildren = {
		visible: {
			opacity: 1,
			height: 29,
		},
		hidden: {
			height: 0,
			opacity: 0,
		},
	};
	const openSecondLevelKey = (key: KeyboardEvent, category: string) => {
		key.preventDefault();
		if (key.code === 'Space' || key.code === 'Enter') openSecondLevel(category);
	};

	const openSecondLevel = (secondCategory: string) => {
		setMenu && setMenu(menu.map(modifiedMenu => {
			if (modifiedMenu._id.secondCategory == secondCategory) {
				setAnnounce(modifiedMenu.isOpened ? "closed" : 'opened');
				modifiedMenu.isOpened = !modifiedMenu.isOpened;
			}
			return modifiedMenu;
		}));
	};
	const buildFirstLevel = () => {
		return <ul className={styles.firstLevelList}>
			{firstLevelMenu.map(menu => {
				return (
					<li key={menu.route} aria-expanded={menu.id === firstCategory}>
						<NextLink href={`/${menu.route}`}>
							<a>
								<span className={cn(styles.firstLevel, {
									[styles.firstLevelActive]: menu.id === firstCategory
								})}>
									{menu.icon}
									<Typography className={styles.firstLevelTrigger} tag="span">
										{menu.name}
									</Typography>
								</span>
							</a>
						</NextLink>
						{Boolean(menu.id === firstCategory) && buildSecondLevel(menu)}
					</li>
				);
			})}
		</ul>;
	};
	const buildSecondLevel = (firstLevelMenu: FirstLevelMenuItem) => {
		return (
		<ul className={styles.secondBlock}>
			{menu.map(menu => {
				if (menu.pages.map(page => page.alias).includes(router.asPath.split('/')[2])) {
					menu.isOpened = true;
				}

				return (
					<li key={menu._id.secondCategory}>
						<button
							onKeyPress={(key: KeyboardEvent) => openSecondLevelKey(key, menu._id.secondCategory)}
							className={styles.secondLevel}
							onClick={() => openSecondLevel(menu._id.secondCategory)}
							aria-expanded={menu.isOpened}
							>
							{menu._id.secondCategory}
						</button>
						<motion.ul
							variants={vatiants}
							initial={menu.isOpened ? 'visible' : 'hidden'}
							animate={menu.isOpened ? 'visible' : 'hidden'}
							layout
							className={cn(styles.secondLevelBlock, {
								[styles.secondLevelBlockOpened]: menu.isOpened,
							})}
						>
							{buildThirdLevel(menu.pages, firstLevelMenu.route, menu.isOpened ?? false)}
						</motion.ul>
					</li>
				);
			})}
		</ul>
		);
	};
	const buildThirdLevel = (pages: PageItem[], route: string, isOpened: boolean) => {
		return (
			pages.map(page => (
				<motion.li variants={vatiantsChildren} key={page.alias}>
					<NextLink href={`/${route}/${page.alias}`}>
						<a aria-current={`/${route}/${page.alias}` === router.asPath ? 'page' : false} tabIndex={isOpened ? 0 : -1} className={cn(styles.thirdLevel, {
							[styles.thirdLevelActive]: `/${route}/${page.alias}` === router.asPath,
						})}>{page.category}</a>
					</NextLink>
				</motion.li>

			))
		);
	};

	return (
		<nav role="navigation" className={styles.menu}>
			{announce && <span role="log" className="visuallyHidden">{announce === 'opened' ? 'развернуто' : "свернуто"}</span>}
			{buildFirstLevel()}
		</nav>
	);
}

export default Menu;

