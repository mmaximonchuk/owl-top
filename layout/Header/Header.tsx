import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import cn from 'classnames';
import { motion } from 'framer-motion';

import { HeaderProps } from './Header.props';
import Sidebar from '../Sidebar/Sidebar';
import { ButtonIcon } from '../../components';

import Logo from '../Logo.svg';

import styles from './Header.module.css';


function Header({ className, ...props }: HeaderProps): JSX.Element {
	const [isMenuOpened, setIsMenuOpened] = useState<boolean>(false);
	const router = useRouter();

	useEffect(() => {
		setIsMenuOpened(false);
	}, [router]);

	const menuVariants = {
		opened: {
			opacity: 1,
			x: 0,
			transition: {
				stiffness: 20
			}
		},
		closed: {
			opacity: 1,
			x: '100%',
		}
	};

	return (
		<header role="banner" className={cn(styles.header, className)} {...props}>
			<Logo className={styles.logo} />
			<ButtonIcon appearance="white" icon={"menu"} onClick={() => setIsMenuOpened(true)} />
			<motion.div
				variants={menuVariants}
				initial={'closed'}
				animate={isMenuOpened ? 'opened' : 'closed'}
				className={styles.mobileMenu}
			>
				<Sidebar />
				<ButtonIcon className={styles.menuClose} appearance="white" icon={"close"} onClick={() => setIsMenuOpened(false)} />
			</motion.div>
		</header>
	);
}

export default Header;

