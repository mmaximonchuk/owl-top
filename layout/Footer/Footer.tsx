import React from 'react';
import NextLink from 'next/link';
import cn from 'classnames';
import { format } from 'date-fns';

import { FooterProps } from './Footer.props';
import { Typography } from '../../components';

import styles from './Footer.module.css';

function Footer({ className, ...props }: FooterProps): JSX.Element {
	return (
		<footer role="contentinfo" className={cn(styles.footer, className)} {...props}>
			<div className={styles.inner}>
				<Typography size="m" tag="p" className={styles.rights}>OwlTop © 2020 - {format(new Date(), 'yyyy')} Все права защищены</Typography>
				<NextLink href="/" >
					<a className={styles.agreement}>Пользовательское соглашение</a>
				</NextLink>
				<NextLink href="/">
					<a className={styles.privacy}>Политика конфиденциальности</a>
				</NextLink>
			</div>
		</footer>
	);
}

export default Footer;

