import React, { FunctionComponent, KeyboardEvent, useRef, useState } from 'react';
import { LayoutProps } from './Layout.props';
import { AppContextProvider, IAppContext } from '../context/app.context';
import cn from 'classnames';
import Header from './Header/Header';
import Sidebar from './Sidebar/Sidebar';
import Footer from './Footer/Footer';

import styles from './Layout.module.css';
import { Up } from '../components';


const Layout = ({ children }: LayoutProps): JSX.Element => {
	const [isSkipedLinkDisplayed, setIsSkipedLinkDisplayed] = useState<boolean>(false);
	const bodyRef = useRef<HTMLDivElement>(null);

	const skipContentAction = (key: KeyboardEvent) => {
		if (key.code === 'Space' || key.code === 'Enter') {
			key.preventDefault();
			bodyRef.current?.focus();
		} 
		setIsSkipedLinkDisplayed(false);
	};

	return (
		<div className={styles.layout}>
			<a
				onFocus={() => setIsSkipedLinkDisplayed(true)}
				onKeyDown={skipContentAction}
				tabIndex={0}
				className={cn(styles.skipLink, {
					[styles.displayed]: isSkipedLinkDisplayed,
				})}
			>Сразу к содержанию</a>

			<Header className={styles.header} />
			<Sidebar className={styles.sidebar} />
			<main role="main" className={styles.main} ref={bodyRef} tabIndex={0}>
				{children}
			</main>
			<Footer className={styles.footer} />
			<Up />
		</div>
	);
};

export const withLayout = <T extends Record<string, unknown> & IAppContext>(Component: FunctionComponent<T>) => {
	return (props: T): JSX.Element => {
		return <AppContextProvider menu={props.menu} firstCategory={props.firstCategory}>
			<Layout><Component {...props} /></Layout>
		</AppContextProvider>;
	};
};
