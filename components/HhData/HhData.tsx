import React from "react";
import cn from "classnames";

import { HhDataProps } from "./HhData.props";

import { Card } from '../Card/Card';
import Star from './Star.svg';
import styles from "./HhData.module.css";
import { priceRu } from '../../helpers/helpers';

export function HhData({
  count,
  juniorSalary,
  middleSalary,
  seniorSalary
}: HhDataProps): JSX.Element {
  return (
    <section className={styles.hh}>
      <Card className={styles.hhCount}>
        <div className={styles.hhStatTitle}>Всего вакансий</div>
        <div className={styles.hhCountValue}>{count}</div>
      </Card>
      <Card className={styles.salary}>
        <div>
          <div className={styles.hhStatTitle}>Начальный</div>
          <div className={styles.salaryValue}>{priceRu(juniorSalary)}</div>
          <div className={styles.rate}><Star className={styles.filled} /><Star /><Star /></div>
        </div>
        <div>
          <div className={styles.hhStatTitle}>Средний</div>
          <div className={styles.salaryValue}>{priceRu(middleSalary)}</div>
          <div className={styles.rate}><Star className={styles.filled} /><Star className={styles.filled} /><Star /></div>
        </div>
        <div>
          <div className={styles.hhStatTitle}>Профессионал</div>
          <div className={styles.salaryValue}>{priceRu(seniorSalary)}</div>
          <div className={styles.rate}><Star className={styles.filled} /><Star className={styles.filled} /><Star className={styles.filled} /></div>
        </div>
      </Card>
    </section>
  );
}

