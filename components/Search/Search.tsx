import React, { KeyboardEvent, useEffect, useState } from "react";
import cn from "classnames";
import { useRouter } from 'next/router';
import { SearchProps } from "./Search.props";

import { Button, Input } from '..';

import SearchIcon from './Search.svg';

import styles from "./Search.module.css";

export function Search({
  className,
  ...restProps
}: SearchProps): JSX.Element {
  const [search, setSearch] = useState<string>('');
  const [searchRequest, setSearchRequest] = useState<boolean>(false);

  const router = useRouter();

  const handleKeyPress = (e: KeyboardEvent) => {
    if (e.key === "Enter") 
      setSearchRequest(!searchRequest);
  };

  const goToSearch = (): void => {
    router.push({
      pathname: '/search',
      query: {
        q: search
      }
    });
  };

  return (
    <form onSubmit={e => e.preventDefault()} className={cn(styles.search, className)} {...restProps} role="search">
      <Input value={search} placeholder="Поиск..." onChange={(e) => setSearch(e.target.value)} onKeyDown={handleKeyPress} />
      <Button 
        appearance="primary" 
        className={styles.button} 
        onClick={() => goToSearch()}
        aria-label="Искать по сайту"
      >
        <SearchIcon />
      </Button>
    </form>
  );
}


