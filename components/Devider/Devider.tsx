import React from "react";
import { DeviderProps } from "./Devider.props";
import cn from 'classnames';


import s from "./Devider.module.css";

export function Devider({
  className,
  ...restProps
}: DeviderProps): JSX.Element {
  return (
    <hr className={cn(s.hr, className)} {...restProps} />
  );
}

