import React, { forwardRef, ForwardedRef, useRef, useState } from "react";
import Image from 'next/image';
import cn from "classnames";
import { motion } from 'framer-motion';

import { Button, Card, Devider, Rating, Review, ReviewForm, Tag } from '..';
import { declinationOfNum, priceRu } from '../../helpers/helpers';
import { ProductProps } from "./Product.props";

import s from "./Product.module.css";

export const Product = motion(forwardRef(({ product, className, ...restProps }: ProductProps, ref: ForwardedRef<HTMLDivElement>): JSX.Element => {
  const [isReviewOpened, setIsReviewOpened] = useState<boolean>(false);
  const reviewRef = useRef<HTMLDivElement>(null);

  const scrollToReview = () => {
    setIsReviewOpened(true);
    reviewRef.current?.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
    reviewRef.current?.focus();
  };

  const cardVariants = {
    visible: {
      height: 'auto',
    },
    hidden: {
      overflow: 'hidden',
      height: 0,
    },
  };

  return (
    <div ref={ref} className={className} {...restProps}>
      <Card className={s.product}>
        <div className={s.logo}>
          {/* <img src={process.env.NEXT_PUBLIC_DOMAIN + product.image} alt={product.title} /> */}
          <Image
            src={`https://courses-top.ru${product.image}`}
            alt={product.title}
            width={70}
            height={70}
            layout="fixed"
          />
        </div>

        <div className={s.title}>{product.title}</div>

        <div className={s.price}>
          <span className='visuallyHidden'>Цена</span>
          {priceRu(product.price)}
          <span className='visuallyHidden'>Скидка</span>
          {product.oldPrice && <Tag className={s.oldPrice} color="green">{priceRu(product.price - product.oldPrice)}</Tag>}
        </div>

        <div className={s.credit}>
          <span className='visuallyHidden'>Кредит</span>
          {priceRu(product.credit)}<span className={s.month}>/мес</span>
        </div>

        <div className={s.rating}>
          <span className='visuallyHidden'>рейтинг {product.reviewAvg ?? product.initialRating}</span>
          <Rating rating={product.reviewAvg ?? product.initialRating} />
        </div>

        <div className={s.tags}>
          {product.categories.map((category) => <Tag key={category} color="ghost" className={s.category}>{category}</Tag>)}
        </div>

        <div className={s.priceTitle} aria-hidden={true}>цена</div>
        <div className={s.creditTitle} aria-hidden={true}>кредит</div>
        <div className={s.rateTitle}><a href="#ref" onClick={scrollToReview}>{product.reviewCount} {declinationOfNum(product.reviewCount, ['отзыв', 'отзыва', 'отзывов'])}</a></div>

        <Devider className={s.hr} />

        <div className={s.description}>{product.description}</div>
        <div className={s.features}>
          {product.characteristics.map(feature => {
            return (
              <div key={feature.name} className={s.characteristics}>
                <span className={s.characteristicsName}>{feature.name}</span>
                <span className={s.characteristicsDots} />
                <span className={s.characteristicsValue}>{feature.value}</span>
              </div>
            );
          })}
        </div>

        <div className={s.advBlock}>
          {!!product.advantages && <div className={s.advantages}>
            <div className={s.advTitle}>
              Преимущества
            </div>
            <div>{product.advantages}</div>
          </div>}
          {!!product.disadvantages && <div className={s.disadvantages}>
            <div className={s.advTitle}>
              Недостатки
            </div>
            <div>{product.disadvantages}</div>
          </div>}
        </div>

        <Devider className={cn(s.hr, s.hr2)} />


        <div className={s.actions}>
          <Button appearance="primary">Узнать подробнее</Button>
          <Button
            onClick={() => setIsReviewOpened(!isReviewOpened)}
            appearance="ghost"
            arrow={isReviewOpened ? 'down' : 'right'}
            className={s.reviewBtn}
            aria-expanded={isReviewOpened}
          >Читать отзывы</Button>
        </div>
      </Card>
      <motion.div
        variants={cardVariants}
        initial={'hidden'}
        animate={isReviewOpened ? 'visible' : 'hidden'}
        transition={{ duration: 0.8 }}
      >
        <Card ref={reviewRef} color="blue" className={s.reviews} tabIndex={isReviewOpened ? 0 : -1}>
          {product.reviews.map(review => {
            return (
              <div key={review._id}>
                <Review review={review} />
                <Devider />
              </div>
            );
          })}
          <ReviewForm isOpened={isReviewOpened} productId={product._id} />
        </Card>
      </motion.div>

    </div>
  );
}));

