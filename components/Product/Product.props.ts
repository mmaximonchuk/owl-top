import { DetailedHTMLProps, InputHTMLAttributes } from 'react';
import { ProductModel } from '../../interface/product.interface';

export interface ProductProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	product: ProductModel;
}
