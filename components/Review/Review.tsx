import React from "react";
import cn from "classnames";
import { format } from "date-fns";
import { ru } from "date-fns/locale";

import { ReviewProps } from "./Review.props";

import UserIcon from './user.svg';

import s from "./Review.module.css";
import { Rating } from '..';

export function Review({
  review,
  className,
  ...restProps
}: ReviewProps): JSX.Element {
  const { name, title, description, createdAt, rating } = review;
  return (
    <div className={cn(s.review, className)} {...restProps}>
      <UserIcon className={s.user} />
      <div className={s.userData}>
        <span className={s.name}>{name}:</span>&nbsp;&nbsp;
        <span>{title}</span>
      </div>
      <div className={s.date}>
        {format(new Date(createdAt), 'dd MMMM yyyy', { locale: ru })}
      </div>
      <div className={s.rating}>
        <Rating rating={rating} />
      </div>
      <p className={s.description}>
        {description}
      </p>
    </div>
  );
}

