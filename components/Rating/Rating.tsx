import React, { ForwardedRef, forwardRef, KeyboardEvent, useEffect, useRef, useState } from "react";
import { RatingProps } from "./Rating.props";
import cn from 'classnames';
import IconStar from "./rating.svg";

import styles from "./Rating.module.css";

export const Rating = forwardRef(({
  className,
  isEditable = false,
  rating,
  setRating,
  error,
  tabIndex,
  ...restProps
}: RatingProps,
  ref: ForwardedRef<HTMLDivElement>): JSX.Element => {
  const [ratingArray, setRatingArray] = useState<JSX.Element[]>(new Array(5).fill(<></>));
  const ratingArrayRef = useRef<(HTMLSpanElement | null)[]>([]);

  useEffect(() => {
    constructRating(rating);
  }, [tabIndex, rating]);

  const changeDisplay = (rating: number) => {
    if (!isEditable) {
      return;
    }
    constructRating(rating);
  };

  const handleClick = (rating: number) => {
    if (!isEditable || !setRating) {
      return;
    }
    setRating(rating);
  };

  const handleKeyPress = (event: KeyboardEvent) => {
    if (!isEditable || !setRating) {
      return;
    }
    if (event.code === "ArrowRight" || event.code === "ArrowUp") {
      event.preventDefault();
      if (!rating) setRating(1);
      else {
        setRating(rating < 5 ? rating + 1 : 5);
      }
      ratingArrayRef.current[rating]?.focus();
    }
    if (event.code === "ArrowLeft" || event.code === "ArrowDown") {
      event.preventDefault();
      if (!rating) setRating(1);
      else {
        setRating(rating > 1 ? rating - 1 : 1);
      }
      ratingArrayRef.current[rating - 2]?.focus();
    }
  };

  const computeFocus = (r: number, i: number):number => {
    if(!isEditable) return -1;
    if (!rating && i == 0) return tabIndex ?? 0;
    if (r === i + 1) return tabIndex ?? 0;
    return -1;
  };
  
  const constructRating = (currentRating: number) => {
    const updatedArray = ratingArray.map((r: JSX.Element, i: number) => {
      return (
        <span className={cn(styles.star, className, {
          [styles.filled]: i < currentRating,
          [styles.editable]: isEditable === true,
        })}
          onMouseEnter={() => changeDisplay(i + 1)}
          onMouseLeave={() => changeDisplay(rating)}
          onClick={() => handleClick(i + 1)}
          tabIndex={computeFocus(rating, i)}
          onKeyDown={handleKeyPress}
          ref={r => ratingArrayRef.current?.push(r)}
          role={isEditable ? 'slider' : ''}
          aria-valuenow={rating}
          aria-valuemax={5}
          aria-valuemin={1}
          aria-label={isEditable ? "Укажите рейтинг" : `Рейтинг ${rating}`}
          aria-invalid={error ? true : false}
        >
          <IconStar />
        </span>

      );
    });
    setRatingArray(updatedArray);
  };
  return (
    <div className={cn(styles.ratingWrapper, className, {
      [styles.error]: error
    })} ref={ref} {...restProps}>
      {ratingArray.map((rating, i) => <span key={i}>{rating}</span>)}
      {error && <p role="alert" className={styles.errorMessage}>{error.message}</p>}
    </div>
  );
});

