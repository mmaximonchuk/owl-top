import { motion, useAnimation } from 'framer-motion';
import React, { useEffect } from 'react';
import { ButtonIcon } from '..';
import { useScrollY } from '../../hooks/useScrollY';
import styles from './Up.module.css';

export function Up(): JSX.Element {
	const control = useAnimation();
	const y = useScrollY();

	useEffect(() => {
		control.start({ opacity: y / document.body.scrollHeight });
	}, [y, control]);

	const scrollToTop = () => {
		window.scrollTo({ top: 0, behavior: 'smooth' });
	};


	return (
		<motion.div
			animate={control}
			initial={{ opacity: 0 }}
			aria-label="Наверх"
			className={styles.up}
		>
			<ButtonIcon icon='up' appearance="primary" onClick={scrollToTop}/>
		</motion.div>
	);
}


