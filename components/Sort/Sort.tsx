import React, { KeyboardEvent } from "react";
import cn from "classnames";
import { SortEnum, SortProps } from "./Sort.props";
import SortIcon from "./sort.svg";
import PriceIcon from "./desc.svg";

import styles from "./Sort.module.css";

export function Sort({ sort, setSort, className, ...restProps }: SortProps): JSX.Element {


  return (
    <div className={cn(styles.sort, className)} {...restProps}>
      <div className={styles.sortName} id="">Сортировка</div>
      <button
        onClick={() => setSort(SortEnum.Rating)}
        className={cn({
          [styles.active]: sort === SortEnum.Rating
        })}
        id=""
        aria-selected={sort === SortEnum.Rating}
        aria-labelledby="sort rating"
      >
        <SortIcon className={styles.sortIcon} /> По рейтингу
      </button>
      <button
        onClick={() => setSort(SortEnum.Price)}
        className={cn({
          [styles.active]: sort === SortEnum.Price
        })}
        id="price"
        aria-selected={sort === SortEnum.Rating}
        aria-labelledby="sort price"
      >
        <PriceIcon className={styles.sortIcon} /> По цене
      </button>
    </div>
  );
}

