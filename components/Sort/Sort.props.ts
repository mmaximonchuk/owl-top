import { DetailedHTMLProps, HTMLAttributes } from 'react';

export interface SortProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	sort: SortEnum,
	setSort: (sort: SortEnum) => void;
}

export enum SortEnum {
	Rating = 'RATING',
	Price = "PRICE",
	ResetProducts = "RESET_PRODUCTS"
}
