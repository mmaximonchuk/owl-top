import React, { ForwardedRef, forwardRef } from "react";
import cn from "classnames";
import { TextareaProps } from "./Textarea.props";

import styles from "./Textarea.module.css";

export const Textarea = forwardRef(({
  className,
  error,
  ...restProps
}: TextareaProps,
  ref: ForwardedRef<HTMLTextAreaElement>): JSX.Element => {
  return (
    <div className={cn(styles.textAreaWrapper, className)}>
      <textarea ref={ref} className={cn(styles.textarea, {
        [styles.error]: error
      })} {...restProps} />
      {error && <p role="alert"  className={styles.errorMessage}>{error.message}</p>}
    </div>
  );
});

