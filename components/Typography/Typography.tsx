import React from "react";
import cn from "classnames";
import { TypographyProps, HtagProps } from "./Typography.props";

import styles from "./Typography.module.css";

export function Typography({
  children,
  className,
  size = "m",
  tag,
  ...restProps
}: TypographyProps): JSX.Element {
  switch (tag) {
    case "p":
      return (
        <p className={cn(styles.p, className, {
          [styles.small]: size === "s",
          [styles.medium]: size === "m",
          [styles.large]: size === "l",
        })} {...restProps}>
          {children}
        </p>
      );
    case "span":
      return (
        <span className={cn(styles.span, className)} {...restProps}>
          {children}
        </span>
      );
    default: return <></>;
  }
}

export function Title({ tag, children, className }: HtagProps): JSX.Element {
  switch (tag) {
    case 'h1':
      return <h1 className={cn(styles.h1, className)}>{children}</h1>;
    case 'h2':
      return <h2 className={cn(styles.h2, className)}>{children}</h2>;
    case 'h3':
      return <h3 className={cn(styles.h3, className)}>{children}</h3>;
    case 'h4':
      return <h4 className={cn(styles.h4, className)}>{children}</h4>;
    default: return <></>;
  }
}