import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface TypographyProps extends DetailedHTMLProps<HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement> {
	size?: 's' | 'm' | "l";
	tag: "p" | "span" | "strong";
	className?: string;
	children: ReactNode;
}
export interface HtagProps {
	tag: "h1" | "h2" | "h3" | "h4";
	className?: string;
	children: ReactNode;
}