import React from "react";
import cn from "classnames";

import { AdvantagesProps } from "./Advantages.props";

import BenefitTick from './benefitTick.svg';
import { Tag, Title, Typography } from '..';

import styles from "./Advantages.module.css";

export function Advantages({ advantages, seoText, tags }: AdvantagesProps): JSX.Element {
  return (
    <div className={styles.coursesBenefits}>
      <Title tag="h2" className={styles.benefitsTitle}>Преимущества</Title>
      <div className={styles.benefits}>
        {advantages?.length && advantages.map(benefit => {
          return (
            <div key={benefit._id} className={styles.benefit}>
              <div className={styles.left}>
                <BenefitTick className={styles.benefitIcon} />
                <div className={styles.devider} />
              </div>
              <div className={styles.right}>
                <Title tag="h3" className={styles.benefitTitle}>
                  {benefit.title}
                </Title>
                {!!benefit.description && <Typography tag="p" size="l" className={styles.benefitPar}>
                  {benefit.description}
                </Typography>}
              </div>
            </div>
          );
        })}

      </div>
      {!!seoText && <Typography tag="p" size="l"><div className={styles.seo} dangerouslySetInnerHTML={{ __html: seoText }} /></Typography>}
      <Title tag="h2" className={styles.skillsTitle}>
        Получаемые навыки
      </Title>
      {!!tags && <div className={styles.skills}>
        {tags.map(tag => <Tag className={styles.skill} key={tag} color="primary">
          {tag}
        </Tag>)}

      </div>}
    </div>
  );
}

