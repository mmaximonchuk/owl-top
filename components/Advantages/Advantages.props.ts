import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { TopPageAdvantage } from '../../interface/page.interface';

export interface AdvantagesProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	advantages?: TopPageAdvantage[];
	seoText?: string;
	tags?: string[];
}
