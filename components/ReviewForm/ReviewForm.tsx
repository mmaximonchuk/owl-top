import React, { useState } from "react";
import { useForm, Controller } from 'react-hook-form';
import cn from "classnames";
import axios from 'axios';
import { API } from '../../helpers/api';

import { IReviewForm, IReviewSentResponse } from './ReviewForm.interface';
import { ReviewFormProps } from "./ReviewForm.props";
import { Button, Input, Rating, Textarea } from '..';

import CloseIcon from './cross.svg';

import s from "./ReviewForm.module.css";
import { motion, AnimatePresence } from 'framer-motion';

export function ReviewForm({
  productId,
  isOpened,
  className,
  ...restProps
}: ReviewFormProps): JSX.Element {
  const { register, control, handleSubmit, formState: { errors }, reset, clearErrors } = useForm<IReviewForm>();
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isError, setError] = useState<string | undefined>();
  const successVariants = {
    visible: {
      opacity: 1,
      x: 0,
    },
    hidden: {
      opacity: 0,
      x: "-100%",
    },
    close: {
      opacity: 0,
      x: "130%",
    }
  };
  const onSubmit = async (formData: IReviewForm) => {
    try {
      const { data } = await axios.post<IReviewSentResponse>(API.review.createDemo, { ...formData, productId });
      if (data.message) {
        setIsSuccess(true);
        reset();
      } else {
        setError('Что-то пошло не так...');
      }
    } catch (e) {
      setError(e.message);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={cn(s.reviewForm, className)} {...restProps}>
        <Input
          {...register('name', { required: { value: true, message: 'Заполните имя' } })}
          error={errors.name}
          className={s.formInput}
          placeholder='Имя'
          tabIndex={isOpened ? 0 : -1}
          aria-invalid={errors.name ? true : false}
        />
        <Input
          {...register('title', { required: { value: true, message: 'Заполните заголовок' } })}
          error={errors.title}
          className={cn(s.title, s.formInput)}
          placeholder='Заголовок отзыва'
          tabIndex={isOpened ? 0 : -1}
          aria-invalid={errors.title ? true : false}

        />
        <div className={s.rating}>
          <span>Оценка:</span>
          <Controller
            control={control}
            name="rating"
            rules={{ required: { value: true, message: 'Укажите рейтинг' } }}
            render={({ field }) => (
              <Rating
                error={errors.rating}
                ref={field.ref}
                rating={field.value}
                isEditable
                setRating={field.onChange}
                tabIndex={isOpened ? 0 : -1}
              />
            )}
          />
        </div>
        <Textarea
          {...register('description', { required: { value: true, message: 'Заполните описание' } })}
          error={errors.description}
          className={s.formDescription}
          tabIndex={isOpened ? 0 : -1}
          placeholder='Текст отзыва'
          aria-label="Текст отзыва"
          aria-invalid={errors.description ? true : false}
        />
        <div className={s.submit}>
          <Button tabIndex={isOpened ? 0 : -1} appearance="primary" onClick={() => clearErrors()}>Отправить</Button>
          <span className={s.info}>* Перед публикацией отзыв пройдет предварительную модерацию и проверку</span>
        </div>
      </div>
      <AnimatePresence>
        {isSuccess &&
          <motion.div
            role="alert"
            variants={successVariants}
            initial={'hidden'}
            animate={isSuccess ? 'visible' : 'hidden'}
            exit={'close'}
            transition={{
              delay: 1,
              x: { type: "spring", stiffness: 100 },
              default: { duration: 1 },
            }}
            className={s.success}
          >
            <div className={s.successTitle}>Ваш отзыв успешно отправлен!</div>
            <p className={s.successDescription}>
              Спасибо, ваш отзыв будет опубликова после проверки
            </p>
            <button aria-label="Закрыть оповещение" className={s.alertBtn}>
              <CloseIcon className={s.close} onClick={() => setIsSuccess(false)} />
            </button>
          </motion.div>}
      </AnimatePresence>
      <AnimatePresence>
        {isError && <motion.div
          role="alert"
          variants={successVariants}
          initial={'hidden'}
          animate={isError ? 'visible' : 'hidden'}
          exit={'close'}
          transition={{
            delay: 1,
            x: { type: "spring", stiffness: 100 },
            default: { duration: 1 },
          }} className={s.error}>
          Что-то пошло не так, попробуйте обновить страницу
          <button aria-label="Закрыть оповещение" className={s.alertBtn}>
            <CloseIcon className={s.close} onClick={() => setError('')} />
          </button>
        </motion.div>}
      </AnimatePresence>
    </form>
  );
}

