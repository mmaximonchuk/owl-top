import React from "react";
import cn from "classnames";
import { TagProps } from "./Tag.props";

import styles from "./Tag.module.css";

export function Tag({
  children,
  className,
  size = "m",
  color = 'ghost',
  href,
  ...restProps
}: TagProps): JSX.Element {
  return (
    <div className={cn(styles.tag, className, {
      [styles.small]: size === "s",
      [styles.medium]: size === "m",
      [styles.ghost]: color === "ghost",
      [styles.red]: color === "red",
      [styles.gray]: color === "gray",
      [styles.green]: color === "green",
      [styles.primary]: color === "primary",
    })} {...restProps}>
      {href ? <a href={href}>{children}</a> : <>{children}</>}
    </div>
  );
}

