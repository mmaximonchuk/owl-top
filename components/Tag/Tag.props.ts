import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface TagProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	size?: 's' | 'm';
	// tag: "p" | "span" | "strong";
	children: ReactNode;
	color?: 'ghost' | 'red' | 'gray' | "green" | "primary";
	href?: string;
}
