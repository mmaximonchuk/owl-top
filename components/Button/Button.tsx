import React, { useEffect } from 'react';
import cn from 'classnames';
import { motion, useMotionValue } from 'framer-motion';
import { ButtonProps } from './Button.props';

import IconArrow from './arrow.svg';

import styles from './Button.module.css';

export function Button({ appearance, arrow = "none", children, className, ...restProps }: ButtonProps): JSX.Element {
	const scale = useMotionValue(1);
	useEffect(() => {
		// scale.onChange(s => console.log(s));
	}, [scale]);

	return (
		<motion.button
			whileHover={{ scale: 1.05 }}
			className={cn(styles.button, className, {
				[styles.primary]: appearance === 'primary',
				[styles.ghost]: appearance === 'ghost',
			})}
			style={{ scale }}
			{...restProps}
		>
			{children}
			{arrow !== 'none' && <span className={cn(styles.arrow, { [styles.down]: arrow === "down" })}>
				<IconArrow />
			</span>}
		</motion.button>
	);
}


