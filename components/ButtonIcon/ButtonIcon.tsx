import React from 'react';
import cn from 'classnames';
import { ButtonIconProps, icons } from './ButtonIcon.props';
// import IconArrow from './arrow.svg';
import styles from './ButtonIcon.module.css';

export function ButtonIcon({ appearance, icon, className, ...restProps }: ButtonIconProps): JSX.Element {
	const IconComponent = icons[icon];
	return (
		<button
			className={cn(styles.button, className, {
				[styles.primary]: appearance === 'primary',
				[styles.white]: appearance === 'white',
			})}
			{...restProps}
		>
			<IconComponent />
		</button>
	);
}


