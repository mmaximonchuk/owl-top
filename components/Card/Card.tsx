import React, { ForwardedRef, forwardRef } from "react";
import cn from "classnames";
import { CardProps } from "./Card.props";

import styles from "./Card.module.css";

export const Card = forwardRef(({
  children,
  className,
  color = "white",
  ...restProps
}: CardProps, 
 ref: ForwardedRef<HTMLDivElement>): JSX.Element => {
  return (
    <div ref={ref} className={cn(styles.card, className, {
      [styles.blue]: color === "blue",
    })} {...restProps}>
      {children}
    </div>
  );
});

