/// <reference types="next" />
/// <reference types="next/types/global" />
/// <reference types="next/image-types/global" />

declare module "*.svg" {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	const content: React.FunctionComponent<React.SVGAttributes<SVGAElement>>;
	export default content;
}
declare module "classnames" {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	const content: any;
	export default content;
}